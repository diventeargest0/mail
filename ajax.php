<?php
$attachmentsSize=0;
$connection=mysqli_connect('DBHOST','DBLOGIN','DBPASSWORD','DBNAME');
function fbytes($bytes,$precision=2){
   	$units=array('B','KB','MB','GB','TB');
   	$bytes=max($bytes,0);
   	$pow=floor(($bytes?log($bytes):0)/log(1024));
   	$pow=min($pow,count($units)-1);
   	$bytes/=pow(1024,$pow);
   	return round($bytes,$precision).' '.$units[$pow];
}
if(isset($_POST['data']['__diskfiles'])){
	foreach($_POST['data']['__diskfiles'] as $fileId){
		if(!empty($fileId)){
			$fileId=substr($fileId,1);
			$request=mysqli_query($connection,'select size from b_disk_object where id="'.$fileId.'";');
			while($row=$request->fetch_assoc()){
				$attachmentsSize+=$row['size'];
			}
		}
	}
}
if($attachmentsSize>10485760){
	$_POST['data']['message'].='<br><hr><br><strong>Прикрепленные файлы:</strong><br><br>';
	$attachmentIndex=1;
	foreach($_POST['data']['__diskfiles'] as $fileId){
		if(!empty($fileId)){
			$fileId=substr($fileId,1);
			$hashId=md5($fileId);
			$request=mysqli_query($connection,'select name,size from b_disk_object where id="'.$fileId.'";');
			while($row=$request->fetch_assoc()){
				$fileName=$row['name'];
				$fileSize=fbytes($row['size']);
			}
			mysqli_query($connection,'delete from b_disk_external_link where hash="'.$hashId.'";');
			mysqli_query($connection,'insert into b_disk_external_link (object_id,hash) values ("'.$fileId.'","'.$hashId.'");');
			$_POST['data']['message'].='<a href="https://EXAMPLE.COM/docs/pub/'.$hashId.'/default/?&" target="_blank">'.$attachmentIndex.') '.$fileName.' ('.$fileSize.')</a><br><br>';
			$attachmentIndex++;
		}
	}
	$_POST['data']['__diskfiles']=array();
}
mysqli_close($connection);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/services/ajax.php");
?>
